// Library Imports
require("dotenv").config();
require("module-alias/register");
const express = require("express");

const bodyParser = require("body-parser");
const http = require("http");
const cors = require("cors");
const services = require("./src/services");

// Project Imports
const database = require("./src/database/connect");
const routes = require("./src/routes");
const { PORT } = require("./src/config/config");


const app = express();


// Connect to database
database
  .connectDB()
  .then(() => {
    console.log("Connected to MongoDB");

    var server = http.createServer(app);
    app.use(cors());


    app.use(bodyParser.json());



    app.use("/", routes);

    app.use((err, req, res, next) => {
      if (err) {
        res.status(400).send({ message: "Error Parsing Data." });
      } else {
        next();
      }
    });

    // let socket = getSocket()
    //console.log(socket)

    server.listen(PORT, () => {
      console.log(`listening to port ${PORT}`);
    });


  })
  .catch((err) => console.log("Error connecting to MongoDB:", err));
