/**
 * @enum {string}
 */
const memberStates = { ACTIVE: "ACTIVE", INACTIVE: "INACTIVE", REMOVED: "REMOVED" };

/**
 * @enum {string}
 */
const groupStates = { ACTIVE: "active", INACTIVE: "inactive", DELETED: "deleted" };

/**
 * @enum {string}
 */
const NOTIFICATION_TYPES = { REMOVE_MEMBER: "remove_from_thought", LIKE_MESSAGE: "liked_message" };



/**
 * @enum {string}
 */
const USER_STATUS = { ACTIVE: "Activated", INACTIVE: "Deactivated", DELETED: "Deleted", BLOCKED: "Blocked" };
const CallStates = { ACTIVE: "ACTIVE", PLACED: "PLACED", REJECTED: "REJECTED", ENDED: "ENDED", MISSED: "MISSED" };

/**
 * @enum {string}
 */
const MESSAGE_TYPES = {
  TEXT: "TEXT",
  IMAGE: "IMAGE",
  GIF: "GIF",
  VIDEO: "VIDEO",
  PDF: "PDF",
  INFO: "INFO",
  AUDIO: "AUDIO",
  FILE: "FILE",
};

/**
 * @enum {PermissionsType}
 */
const permissions = { READ: "Read", WRITE: "Write" };

const NOT_MEDIA_TYPES = { TEXT: MESSAGE_TYPES.TEXT, INFO: MESSAGE_TYPES.INFO, AUDIO: MESSAGE_TYPES.AUDIO };

module.exports = {
  memberStates,
  groupStates,
  NOTIFICATION_TYPES,
  MESSAGE_TYPES,
  NOT_MEDIA_TYPES,
  permissions,
  USER_STATUS,
  CallStates
};
