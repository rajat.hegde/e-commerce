const UserServiceClass = require("./UserService");
const AddressServiceClass = require("./addressService");
const ProductServiceClass = require("./productService");

const UserService = new UserServiceClass();
const AddressService = new AddressServiceClass();
const ProductService = new ProductServiceClass();

module.exports = {
    UserService,
    AddressService,
    ProductService
}