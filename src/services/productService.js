const product = require("../database/models/product");
const productorders = require("../database/models/order")
const { Types } = require("mongoose");

class productService {
    constructor() { }

    async createproduct(userData) {

        const updateproduct = await product.create(
            { ...userData }
        );
        return updateproduct;
    }

    async createorder(userData) {

        const updateproduct = await productorders.create(
            { ...userData }
        );
        return updateproduct;
    }

    getproductByUniqueId(uniqueproductID) {
        return product.findOne({ _id: uniqueproductID });
    }

    async getByConstraints(constraints) {
        return await product.find({ ...constraints }).populate('users');
    }

    async getallproductsofuser(query) {
        console.log(query)
        return await product.aggregate([
            { $match: { userID: Types.ObjectId(query.userID) } },
            { $sort: { createdAt: -1 } },
            {
                $lookup: {
                    from: "users",
                    let: { userID: "$userID" },
                    pipeline: [
                        { $match: { $expr: { $eq: ["$_id", "$$userID",] } } }
                    ],
                    as: "user"
                }
            },
            { $unwind: { path: "$user" } },
            {
                $lookup: {
                    from: "addresslists",
                    localField: "userID",
                    foreignField: "userID",
                    as: "address"
                }
            },
        ]);
    }

    async getallproductsdatefilter(query) {
        console.log(query)
        return await productorders.aggregate([
            { $match: { createdAt: { $gte: new Date(query.from), $lte: new Date(query.to) } } },
            { $sort: { createdAt: -1 } },
            {
                $lookup: {
                    from: "users",
                    let: { userID: "$userID" },
                    pipeline: [
                        { $match: { $expr: { $eq: ["$_id", "$$userID",] } } }
                    ],
                    as: "user"
                }
            },
            { $unwind: { path: "$user" } },
            {
                $lookup: {
                    from: "addresslists",
                    localField: "userID",
                    foreignField: "userID",
                    as: "address"
                }
            },

        ]);
    }

    async getallproductsordersofuser(query) {
        console.log(query)
        return await productorders.aggregate([
            { $match: { product_id: Types.ObjectId(query.product_id) } },
            { $sort: { createdAt: -1 } },
            {
                $lookup: {
                    from: "users",
                    let: { userID: "$userID" },
                    pipeline: [
                        { $match: { $expr: { $eq: ["$_id", "$$userID",] } } }
                    ],
                    as: "user"
                }
            },
            { $unwind: { path: "$user" } },
            {
                $lookup: {
                    from: "addresslists",
                    localField: "userID",
                    foreignField: "userID",
                    as: "address"
                }
            },

        ]);
    }


}

module.exports = productService;