const User = require("../database/models/user");

const changableValues = ["username", "fullname", "status"];

class UserService {
  constructor() { }

  async createUser(userData) {

    const updateUser = await User.create(
      { ...userData }
    );
    return updateUser;
  }

  getUserByUniqueId(uniqueUserID) {
    return User.findOne({ _id: uniqueUserID });
  }

  async getByConstraints(constraints) {
    return await User.find({ ...constraints });
  }

  async setOnline(userID) {
    return await User.findOneAndUpdate({ _id: userID }, { $set: { online: true } }, { new: true });
  }
  async setOffline(userID) {
    return await User.findOneAndUpdate({ _id: userID }, { $set: { online: false } }, { new: true });
  }
}

module.exports = UserService;
