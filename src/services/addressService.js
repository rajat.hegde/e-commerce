const Address = require("../database/models/address");

class AddressService {
    constructor() { }

    async createAddress(userData) {

        const updateAddress = await Address.create(
            { ...userData }
        );
        return updateAddress;
    }

    getAddressByUniqueId(uniqueAddressID) {
        return Address.findOne({ _id: uniqueAddressID });
    }

    async getByConstraints(constraints) {
        return await Address.find({ ...constraints });
    }


}

module.exports = AddressService;