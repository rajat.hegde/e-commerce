/*2. find all products of a user
both with user and product information(all)*/
/*1. find all users who ordered a specific product

3. find products ordered in a specific date range
*/
const { ProductService } = require("../services")

const getAllproducts = async (req, res) => {
    try {
        let query = {
            userID: req.query.userID
        }
        const products = await ProductService.getallproductsofuser(query);
        res.status(200).send({ message: "success", products });
    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
}
const getAllorders = async (req, res) => {
    try {
        let query = {
            product_id: req.query.product_id
        }
        const products = await ProductService.getallproductsordersofuser(query);
        res.status(200).send({ message: "success", products });
    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
}


const getAllordersByDate = async (req, res) => {
    try {
        let query = {
            from: req.query.from,
            to: req.query.to
        }
        const products = await ProductService.getallproductsdatefilter(query);
        res.status(200).send({ message: "success", products });
    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
}

module.exports = {
    getAllproducts,
    getAllorders,
    getAllordersByDate
}