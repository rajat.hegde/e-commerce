const user = require("../database/models/user")
const product = require("../database/models/product")
const address = require("../database/models/address")
const { UserService, AddressService, ProductService } = require("../services")


const createUser = async (req, res) => {
    try {
        const data = req.body;
        const user = await UserService.createUser(data);
        res.status(200).send({ message: "success", user });

    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
};

const addAddress = async (req, res) => {
    try {
        const data = req.body;
        data.userID = req.query.userID;
        const user_address = await AddressService.createAddress(data);
        res.status(200).send({ message: "success", user_address });

    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
};

const addproduct = async (req, res) => {
    try {
        const data = req.body;
        data.userID = req.query.userID;
        const user_product = await ProductService.createproduct(data);
        res.status(200).send({ message: "success", user_product });

    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
};

const placeorder = async (req, res) => {
    try {
        const data = req.body;
        data.userID = req.query.userID;
        const placed = await ProductService.createorder(data);
        res.status(200).send({ message: "success", placed });

    } catch (error) {
        res.status(500).send({ message: error.message, errorType: error.name });

    }
};

module.exports = {
    createUser,
    addAddress,
    addproduct,
    placeorder
}
