// Library Imports
const express = require("express");
const cors = require("cors");

// Router Imports

const { createUser, addAddress, addproduct, placeorder } = require("../controllers/insertdata");
const { getAllproducts, getAllorders, getAllordersByDate } = require("../controllers/products")

const router = express.Router().use(cors());

router.post("/user", createUser);
router.post("/address", addAddress);
router.post("/product", addproduct);
router.post("/order", placeorder);

router.get("/allproducts", getAllproducts);
router.get("/allorders", getAllorders);
router.get("/order/datefilter", getAllordersByDate);

router.get("/health", (req, res) => {
  res.json({ status: "HEALTHY" });
});

module.exports = router;
