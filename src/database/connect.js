// Library Imports
var mongoose = require("mongoose");

// Project Imports
const { MongoDBUrl } = require("./../config/config");

const connectDB = () => {
  return mongoose.connect(MongoDBUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
};

module.exports = { connectDB };
