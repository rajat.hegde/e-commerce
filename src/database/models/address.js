// Library Imports
const mongoose = require("mongoose");
const { USER_STATUS } = require("../../commons/enums");

/**
 * @constructor Address
 */
const AddressSchema = mongoose.Schema({
    userID: { type: mongoose.Types.ObjectId, trim: true, ref: "users" },
    address_line1: { type: String, trim: true },
    address_line2: { type: String, trim: true },
    landmark: { type: String, trim: true },
    state: { type: String, trim: true },
    country: { type: String, trim: true },
    pincode: { type: String, trim: true },
    status: { type: String, enum: Object.values(USER_STATUS), default: USER_STATUS.ACTIVE },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
});

const Address = mongoose.model("addresslist", AddressSchema);

module.exports = Address;
