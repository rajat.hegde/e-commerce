// Library Imports
const mongoose = require("mongoose");
const { USER_STATUS } = require("../../commons/enums");

/**
 * @constructor User
 */
const UserSchema = mongoose.Schema({
  username: { type: String, unique: true, trim: true, required: true },
  status: { type: String, enum: Object.values(USER_STATUS), default: USER_STATUS.ACTIVE },
  fullname: { type: String, trim: true },
  lastname: { type: String, trim: true },
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
});

const User = mongoose.model("user", UserSchema);

module.exports = User;
