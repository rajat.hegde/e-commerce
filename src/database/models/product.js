// Library Imports
const mongoose = require("mongoose");
const { USER_STATUS } = require("../../commons/enums");

/**
 * @constructor Product
 */
const ProductSchema = mongoose.Schema({
    userID: { type: mongoose.Types.ObjectId, trim: true, ref: "users" },
    name: { type: String, trim: true },
    description: { type: String, trim: true },
    price: { type: String, trim: true },
    rating: { type: Number, default: 0 },
    status: { type: String, enum: Object.values(USER_STATUS), default: USER_STATUS.ACTIVE },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
});

const Product = mongoose.model("product", ProductSchema);

module.exports = Product;