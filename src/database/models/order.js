// Library Imports
const mongoose = require("mongoose");
const { USER_STATUS } = require("../../commons/enums");

/**
 * @constructor order
 */
const orderSchema = mongoose.Schema({
    userID: { type: mongoose.Types.ObjectId, trim: true, ref: "users" },
    product_id: { type: mongoose.Types.ObjectId, trim: true, ref: "products" },
    description: { type: String, trim: true },
    status: { type: String, enum: Object.values(USER_STATUS), default: USER_STATUS.ACTIVE },
    createdAt: { type: Date, default: Date.now() },
    updatedAt: { type: Date, default: Date.now() },
});

const order = mongoose.model("order", orderSchema);

module.exports = order;