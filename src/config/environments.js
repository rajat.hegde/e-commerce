const environments = { DEV: "development", TEST: "test", PROD: "production" };

module.exports = environments;
