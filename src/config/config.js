const PROD_CONFIG = require("./environment_config/prod.json");
const TEST_CONFIG = require("./environment_config/test.json");
const DEV_CONFIG = require("./environment_config/dev.json");
const LOCAL_CONFIG = require("./environment_config/local.json");
const environments = require("./environments");
const COMMON_CONFIG = require("./commons");
function getEnvironmentConfiguration(NODE_ENV) {
  switch (NODE_ENV) {
    case environments.DEV:
      return DEV_CONFIG || {};
    case environments.TEST:
      return TEST_CONFIG || {};
    case environments.PROD:
      return PROD_CONFIG || {};
    default:
      return LOCAL_CONFIG;
  }
}
const NODE_ENV = process.env.NODE_ENV;
const APP_CONFIG = {
  ...getEnvironmentConfiguration(NODE_ENV),
};

const config = { ...APP_CONFIG, ...COMMON_CONFIG };
module.exports = config;
